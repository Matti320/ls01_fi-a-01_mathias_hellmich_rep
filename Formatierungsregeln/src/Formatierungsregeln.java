
public class Formatierungsregeln {

	public static void main(String[] args) {				//Aufgabe 1
		
		String s = "Java-Programm";
		System.out.printf("\n|%s|\n", s);		//Standartausgabe
		System.out.printf("\n|%20s|\n", s);		//rechtsbündig 20 Stellen
		System.out.printf("\n|%-20s|\n", s);	//linksbündig 20 Stellen
		System.out.printf("\n|%5s|\n", s);		//minimal 5 Stellen 
		System.out.printf("\n|%.4s|\n", s);		//maximal 4 Stellen
		System.out.printf("\n|%20.4s|\n", s);	//20 Positionen rechtsbündig, höchstens 4 Stellen von String
		
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		
		String x = "**";
		String x1 = "*";
		
		System.out.printf("\n%4s%s", x1, x1);		// Möglichkeit 1 (mit x1)
		System.out.printf("\n%s%7s", x1, x1);     	// ACHTUNG bei Eingabe \n%s7s
		System.out.printf("\n%s%7s", x1, x1);
		System.out.printf("\n%5s", x);				// Möglichkeit 2 (mit x)
		
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		String N0 = "0!";					String N6 = "1";			String N12 = "24";
		String N1 = "1!";					String N7 = "2";			String N13 = "120";
		String N2 = "2!";					String N8 = "3";			String N14 = "=";
		String N3 = "3!";					String N9 = "4";			String N15 = "*";
		String N4 = "4!";					String N10 = "5";
		String N5 = "5!";					String N11 = "6";
		
		
		
		
		
		System.out.println("Aufgabe 2");			//Aufgabe 2
		
		System.out.printf("%-5s%-19s%s%4s\n",N0,N14,N14,N6);
		System.out.printf("%-5s%s%-18s%s%4s\n",N1,N14,N6,N14,N6);
		System.out.printf("%-5s%s%s%s%-16s%s%4s\n",N2,N14,N6,N15,N7,N14,N7);
		System.out.printf("%-5s%s%s%s%s%s%s%14s%4s\n",N3,N14,N6,N15,N7,N15,N8,N14,N11);
		System.out.printf("%-5s%s%s%s%s%s%s%s%s%12s%4s\n",N4,N14,N6,N15,N7,N15,N8,N15,N9,N14,N12);
		System.out.printf("%-5s%s%s%s%s%s%s%s%s%s%s%10s%4s\n",N5,N14,N6,N15,N7,N15,N8,N15,N9,N15,N10,N14,N13);
		
		
		
		System.out.println();
		System.out.println();
		
		System.out.println("Aufgabe 3");   //Aufgabe 3
		
		
		System.out.println();
		System.out.println();
		
		String F =("Fahrenheit");			String Dash =("|");							String pos =("+");
		String C =("Celsius");				String Line =("----------------------");
		
		int F1 = -20;			double C1 = -28.8889;		
		int F2 = -10;			double C2 = -23.3333;		
		int F3 = +0;			double C3 = -17.7778;
		int F4 = +20;			double C4 = -6.6667;
		int F5 = +30;			double C5 = -1.1111;
		
		System.out.printf("%-11s%s%9s\n",F,Dash,C);
		System.out.printf("%s\n",Line);
		System.out.printf("%d%9s%9.2f\n",F1,Dash,C1);
		System.out.printf("%d%9s%9.2f\n",F2,Dash,C2);
		System.out.printf("%s%d%10s%9.2f\n",pos,F3,Dash,C3);
		System.out.printf("%s%d%9s%9.2f\n",pos,F4,Dash,C4);
		System.out.printf("%s%d%9s%9.2f\n",pos,F5,Dash,C5);
		
		
		
	}
}
		
		
		