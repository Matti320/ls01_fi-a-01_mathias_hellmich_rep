  package uebungen;
import java.util.Scanner;

public class Methodenuebung {

	public static void main(String[] args) {
		int zahl1=5;
		int zahl2=9;
		System.out.println("Das Ergebnis lautet: "+berechnung2(berechnung(zahl1, intx()))); //r�ckgabewert an eine andere methode �bergeben
//		ausgabe();	
		int erg=1+(berechnung(1,2)); 		  //=erg
		System.out.println(berechnung(1,2));  //ausgeben
		int var= berechnung(3,5);			 //zwischenspeichern
		int e2=var+2;						 //mit dem r�ckgabewert weiterrechnen
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("gebe eine zahl ein:");
		System.out.println("das ergebnis lautet: "+ (berechnung(5,eingabe.nextInt())));
	}
	public static int intx() {
		int z1=3-1;
		return z1;
	}
	public static void ausgabe() {
		System.out.println("test");
	
	}							   //int x, int y sind PARAMETER der methode berechnung und nur innerhalb von berechnung mit den namen x, y bekannt
								   //beim aufruf von berechnung m�ssen 2 int werte �bergeben werden die die methode dann als x und y interpretiert und mit denen sie rechnen kann
								//beim aufruf von berechnung(); ist es egal "wie" die 2 int werte �bergeben werden, direkt als integer zahlenwert (z.b. 3) oder als andere variable(z.b.zahl1) oder als r�ckgabewert einer anderen methode(z.b.intx())
		public static int berechnung(int x, int y) { //Variablendeklarationen gelten nur in der jeweiligen Methode, andere Methoden kennen diese Variablen gar nicht
			
			int erg = x+y;
			return erg;	
		}
		
		public static double berechnung2(int ergebnisDerMethodeBerechnung) {
			
			return Math.sqrt(ergebnisDerMethodeBerechnung*2);
		}
		
		
		
		

	

}
