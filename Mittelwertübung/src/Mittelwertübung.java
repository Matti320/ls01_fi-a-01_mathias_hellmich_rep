
import java.util.Scanner;

public class Mittelwertübung
{

    public static void main(String[] args) {

        //Deklaration

        double input1;
        double input2;
        double result;

        //Eingabe
        Scanner meinscanner = new Scanner(System.in);

        System.out.println("Gib die erste Zahl ein.. ");
        input1 = meinscanner.nextDouble();

        System.out.println("Gib die zweite Zahl ein.. ");
        input2 = (meinscanner.nextDouble());

        //Berechnung

        result=(input1+input2)/2;

        // Ausgabe
         System.out.println("Die Lösung ist " + result);
    }
}