package Auto;

public class Fahrzeug {

	private String marke;
	private int sitze;
	private String farbe;
	private int fuel;
	
	
	public Fahrzeug() {}
	
	public Fahrzeug (String marke, String farbe,int sitze, int fuel) {
		this.marke=marke;
		this.farbe=farbe;
		this.sitze=sitze;
		this.fuel=fuel;
	}


	public String getMarke() {
		return marke;
	}


	public void setMarke(String marke) {
		this.marke = marke;
	}


	public int getSitze() {
		return sitze;
	}

	
	public void setSitze(int sitze) {
		this.sitze = sitze;
	}


	public String getFarbe() {
		return farbe;
	}


	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}


	public int getFuel() {
		return fuel;
	}


	public void setFuel(int fuel) {
		this.fuel = fuel;
	}
	
	public int tanken() {
		this.fuel=70;
		return fuel;
	}
	
	public void checkFuel(int fuel) {
		if (fuel<4) {
			System.out.println("Bitte tanken !!!");}
			else {
				System.out.println("Kraftstoffstand OK");}

			
		
	}
}
	
