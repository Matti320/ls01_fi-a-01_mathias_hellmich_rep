import java.util.Scanner;

public class Mathe {

	public static double eingabe(String text) {
        double input;
        
        Scanner meinscanner = new Scanner(System.in);

        System.out.println(text);
        input = (meinscanner.nextDouble());
        return input;
    }
	
	public static double quadrat(double x) {
		return x*x;
	}
	
	/**public static double hypothenuse(double kathete1,double kathete2) {
		double z1, z2;
		z1=quadrat(kathete1);
		z2=quadrat(kathete2);
		return Math.sqrt(z1+z2);
		//return Math.sqrt(quadrat(kathete1)+quadrat(kathete2) ;
	}	
	**/
	
	public static void ausgabe(double ausgabevariable){
        // Ausgabe
        System.out.println("Das Quadrat ist " + ausgabevariable);
    }

	
	public static void main(String[] args) {
	
		
		double zahl1;
		double ergebnis;
		zahl1 = eingabe("Nenne mir eine Zahl");
		ergebnis = quadrat(zahl1);
		ausgabe(ergebnis);

	}

}
