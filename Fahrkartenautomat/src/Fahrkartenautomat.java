﻿import java.util.Scanner;

class Fahrkartenautomat
{

	public static void main(String[] args)
	    {																		// deklaration der paramaeter + Scanner initialisierung
	       Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag=fahrkartenbestellungErfassen();			// (1)		(speicherort für den returnwert der methode)
	       double eingezahlterGesamtbetrag;									//werden nur in methode verwendet, könnte auch aus der main entfernt werden
	       double eingeworfeneMünze;										//werden nur in methode verwendet, könnte auch aus der main entfernt werden
	       double rückgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);		// (2)
	       byte anzahlTickets;													//änderung in byte ---> weniger speicherplatz, werden nur in methode verwendet, könnte auch in der main entfernt werden	
	       int millisekunde = 0;											// wert für zusätzliche methode
	       
	       
	       
	       
	       fahrkartenAusgeben();								// (3)
	       warte(millisekunde);									// zusätzliche methode
	       rueckgeldAusgeben(rückgabebetrag);					// (4)
	       muenzeAusgeben(rückgabebetrag);						// zusätzliche methode
	       warte2(millisekunde);								// zusätzliche methode geschrieben
	       neuebestellung();									// zusätzliche methode geschrieben
	       warte2(millisekunde);
	       main(args);											//lösungsmöglichkeit (durch recherche im www) 
	    }

	


																									// bestellung erfassen (methode)
	  public static double  fahrkartenbestellungErfassen() {										// Methode komplett umgeschrieben
		   Scanner tastatur = new Scanner(System.in);													// switch case und while schleifen verwendet
		   System.out.println("Fahrkartenbestellvorgang:");
		   System.out.println("=========================");
		   
		   String ticketname []=new String[10];
		   double preis []=new double[10];
		   String auswahl [] = new String[10];
		   
		   ticketname[0]="Einzelfahrschein Berlin AB";												//arrays erstellt für 
		   ticketname[1]="Einzelfahrschein Berlin BC";												// ticketname, preis und auswahl
		   ticketname[2]="Einzelfahrschein Berlin ABC";
		   ticketname[3]="Kurzstrecke";
		   ticketname[4]="Tageskarte Berlin AB";
		   ticketname[5]="Tageskarte Berlin BC";
		   ticketname[6]="Tageskarte Berlin ABC";
		   ticketname[7]="Kleingruppen-Tageskarte Berlin AB";
		   ticketname[8]="Kleingruppen-Tageskarte Berlin BC";
		   ticketname[9]="Kleingruppen-Tageskarte Berlin ABC";
		   
		   preis [0]=2.90;
		   preis [1]=3.30;
		   preis [2]=3.60;
		   preis [3]=1.90;
		   preis [4]=8.60;
		   preis [5]=9.00;
		   preis [6]=9.60;
		   preis [7]=23.50;
		   preis [8]=24.30;
		   preis [9]=24.90;
		   
		   auswahl[0]="[1]";
		   auswahl[1]="[2]";
		   auswahl[2]="[3]";
		   auswahl[3]="[4]";
		   auswahl[4]="[5]";
		   auswahl[5]="[6]";
		   auswahl[6]="[7]";
		   auswahl[7]="[8]";
		   auswahl[8]="[9]";
		   auswahl[9]="[10]";
		   
		   double gesamtpreis=0;
		   boolean bestellen = true;
		   while (bestellen) {
		   System.out.println("\nWählen Sie :\n");
		   
		   for (int i=0; i<preis.length; i++) {																	//for schleife um array auszugeben, 
			   																									// "preis-array" als referenz genommen
		   System.out.printf("%-35s " + "  %10.2f Euro" + "  %5s\n", ticketname[i],preis[i],auswahl[i]);		//auswahl ueber arrays
		   }
		   System.out.println("\nBezahlen                                                [11]");
		   
		   System.out.print("\nIhre Wahl:");
		   int ticket=tastatur.nextInt();

		   while (ticket<1||ticket>11) {																	//falscheingabe abgesichert, 
			   System.out.println(">>>> Falsche Eingabe <<<<");												//inhalt vom default case entfernt
			   System.out.print("\nIhre Wahl:");
			   ticket=tastatur.nextInt();
			   
		   }
		   switch (ticket) {
		   case 1:
			   System.out.print("Anzahl der Tickets: ");
			   byte anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[0];
			   break;
			   
		   case 2:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[1];
			   break;
			   
		   case 3:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[2];
			   break;   

		   case 4:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[3];
			   break;  
			   
		   case 5:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[4];
			   break;   

		   case 6:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[5];
			   break;  

		   case 7:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[6];
			   break;  
			   
		   case 8:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[7];
			   break;		  
			   		  
		   case 9:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[8];
			   break; 
			   
		   case 10:
			   System.out.print("Anzahl der Tickets: ");
			   anzahl=tastatur.nextByte();	
			   
			   while (anzahl<1||anzahl>10){
				   System.out.println("Ungültige Eingabe (maximal 10 Tickets!)");
				   anzahl=tastatur.nextByte();}
			   		  gesamtpreis+=anzahl*preis[9];
			   break;     
			 		   
		   case 11: bestellen = false;
		   break;
			   
		   default:																							//inhalt vom default-case entfernt
			   																								//da absicherung ueber while-schleife erfolgt (weiter oben)
		   }  
		   }
			return gesamtpreis;}												
	       
	     
		
		
	    
		public static double fahrkartenBezahlen(double zuZahlen) {											//auslagerung in methode
			Scanner tastatur = new Scanner(System.in);														//parameterübernahme (zuZahlen) aus vorheriger methode
		       double eingezahlterGesamtbetrag = 0.0;															//als lokale variable deklariert
		       while(eingezahlterGesamtbetrag < zuZahlen)
		       {
		    	   System.out.printf("Noch zu zahlen: " + "%.2f" + " €%n", (zuZahlen - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		    	   double eingeworfeneMünze = tastatur.nextDouble();
		           eingezahlterGesamtbetrag += eingeworfeneMünze;}												//berechnung rückgabebetrag
		           double rückgabebetrag=eingezahlterGesamtbetrag-zuZahlen;
		           return rückgabebetrag;																		//rueckgabewert der methode
		       }
		
		
		
		
		
		public static void fahrkartenAusgeben() {												//methode ohne rückgabewert
		       System.out.println("\nFahrschein wird ausgegeben");
		}
		
		
		
		
		public static void warte(int millisekunde) {										// methode warte 1, waehrend ticket "gedruckt" wird
		       for (int i = 0; i < 8; i++)													// for (zaehl)-schleife für die  "="
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
		}
		
		
		public static void warte2(int millisekunde) {						//neue methode, (parameter) aus main-methode uebernommen
		       for (int i = 0; i < 3; i++)									// zaehlschleife (for) für den neustart des automaten nach abschluss 
		       {															// des kaufvorgangs
		          System.out.println("***");
		          try {
					Thread.sleep(1000);										// zeit hochgesetzt um bessere lesbarkeit zu erzielen
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}}
		      
		
		
		
		public static void rueckgeldAusgeben(double rückgabebetrag){									//methode (ohne returnwert) zur information der
		    																							//rueckgeldausgabe, parameterübernahme aus vorheriger methode
			if(rückgabebetrag > 0.04)
		       {
		    	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO ",rückgabebetrag );
		    	   System.out.println("wird in folgenden Münzen ausgezahlt:");
		    	   }
			else {System.out.println("Danke, dass Sie passend gezahlt haben !");}					//if-else schleife - je nachdem ob rueckgeld
				}																					// ausgegeben wird oder nicht
			
			
		
		
			public static void muenzeAusgeben(double rückgabebetrag) {    			//methode zur muenzausgabenberechnung
			if(rückgabebetrag > 0.0)												//parameterübernahme aus vorheriger methode
																					//geldausgabe nur bei ruekgabe>0 (if - bedingung)
	
		           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen						//berechnung ueber while-schleife
		           {
		        	  System.out.println("2 EURO");
			          rückgabebetrag -= 2.0;
		           }
		           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
		           {
		        	  System.out.println("1 EURO");
			          rückgabebetrag -= 1.0;
		           }
		           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
		           {
		        	  System.out.println("50 CENT");
			          rückgabebetrag -= 0.5;
		           }
		           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
		           {
		        	  System.out.println("20 CENT");
		 	          rückgabebetrag -= 0.2;
		           }
		           while(rückgabebetrag >= 0.09) // 10 CENT-Münzen
		           {
		        	  System.out.println("10 CENT");
			          rückgabebetrag -= 0.1;
		           }
		           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
		           {
		        	  System.out.println("5 CENT");
		 	          rückgabebetrag -= 0.05;
		           }
		           
		           System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n");
		       }
			
			public static void neuebestellung() {									//neue methode - nach abschluss des kaufvorgangs
				System.out.println("Bitte warten....");								//methode ohne rückgabe...nur textausgabe
				System.out.println("Neue Bestellung wird vorbereitet");}
		}
		
		
		