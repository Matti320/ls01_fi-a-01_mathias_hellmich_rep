import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		
		int jahr;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine Jahreszahl ein:");
		jahr = tastatur.nextInt();
		
		if (jahr>1582)
			
			if (jahr%4==0&&jahr%100!=0)							//Regel 1+2 - Jahr durch 4 teilbar aber nicht durch 100
				System.out.print(jahr+" ist ein Schaltjahr");
				  	
			if (jahr%400==0)									//Regel 3 - Jahr durch 400 teilbar
				System.out.print(jahr+" ist ein Schaltjahr");  	
			
			if  (jahr%4!=0)										// Jahr nicht durch 4 teilbar -kein schaltjahr
				System.out.print(jahr+" ist kein Schaltjahr");
							
			if (jahr%100==0&&jahr%400!=0)						// Jahr durch 100 teilbar und nicht durch 400 teilbar
				System.out.print(jahr+" ist kein Schaltjahr");  
		
		if (jahr<1582&&jahr%4==0)								// Ausnahme vor 1582 (aber durch 4 teilbar)
			System.out.print(jahr+" ist ein Schaltjahr");		
		
		
		
		 
			

}
}