/**
 * Diese Klasse modelliert eine Ladung
 * 
 * @author Mathias Hellmich
 * @version 1 vom 28.04.2021
 *
 */
public class Ladung {

	private String bezeichnung;
	private int menge;

	/**
	 * Parameterloser Konstruktor der Klasse Ladung
	 */
	public Ladung() {
	}

	/**
	 * Vollparametrisierter Konstruktor der Klasse Ladung
	 * 
	 * @param bezeichnung Name der Ladung
	 * @param menge       Menge der Ladung
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	/**
	 * Gibt die Bezeichnung der Ladung zur�ck
	 * 
	 * @return gibt die Bezeichnung der Ladung vom Datentyp String zur�ck
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Setzt die Bezeichnung der Ladung
	 * 
	 * @param bezeichnung Name der Ladung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * Gibt die Ladungsmenge zur�ck
	 * 
	 * @return gibt die Menge der Ladung vom Datentyp int zur�ck
	 */
	public int getMenge() {
		return menge;
	}

	/**
	 * Setzt die Menge der Ladung
	 * 
	 * @param menge Menge der Ladung
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}

}
