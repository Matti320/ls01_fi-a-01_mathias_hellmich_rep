import java.util.ArrayList;

/**
 * Diese Klasse modelliert ein Raumschiff und zugehoerige Methoden.
 * 
 * @author Mathias Hellmich
 * @version 1.1 vom 02.05.2021
 *@since JavaDoc - Kommentare upgedated
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	/**
	 * Parameterloser Konstruktor der Klasse Raumschiff
	 */
	public Raumschiff() {
	}

	/**
	 * Vollparametrisierter Konstruktor der Klasse Raumschiff
	 * 
	 * @param photonentorpedoAnzahl                  Anzahl der Photonentorpedos
	 * @param energieversorgungInProzent             Energievesorgung des
	 *                                               Raumschiffes in Prozent
	 * @param zustandSchildeInProzent                Zustand des Schildes in Prozent
	 * @param zustandHuelleInProzent                 Zustand der H�lle in Prozent
	 * @param zustandLebenserhaltungssystemInProzent Zustand des
	 *                                               Lebenserhaltungssystemes in
	 *                                               Prozent
	 * @param androidenAnzahl                        Anzahl der Androiden
	 * @param schiffsname                            Name des Raumschiffes
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemInProzent, int androidenAnzahl,
			String schiffsname) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;

	}

	/**
	 * Gibt die Anzahl der Photonentorpedos zurueck
	 * @return gibt die Anzahl der Photonentorpedos vom Datentyp int zurueck
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	/**
	 * Setzt die Anzahl der Photonentorpedos
	 * @param photonentorpedoAnzahlNeu Anzahl der Photonentorpedos
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	/**
	 * Liefert den Wert der Energieversorgung
	 * @return gibt den Wert der Energieversorgung in Prozent des Raumschiffes vom
	 *         Datentyp int zur�ck
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * Setzt den Wert der Energieversorgung
	 * @param energieversorgungInProzentNeu Energieversorgung in Prozent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	/**
	 * Liefert den Wert des Schildes in Prozent zurueck
	 * @return gibt den Wert des Schildes in Prozent des Raumschiffes vom Datentyp
	 *         int zur�ck
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	/**
	 * Setzt den Wert des Schildes in Prozent
	 * @param zustandSchildeInProzentNeu Zustand des Schildes in Prozent
	 */
	public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
		this.schildeInProzent = zustandSchildeInProzentNeu;
	}

	/**
	 * Liefert den Wert der Huelle in Prozent
	 * @return gibt den Wert der Huelle in Prozent des Raumschiffes vom Datentyp int
	 *         zur�ck
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	/**
	 * Setzt den Wert der Huelle in Prozent
	 * @param zustandHuelleInProzentNeu Zustand der Huelle in Prozent
	 */
	public void setHuelleInProzent(int zustandHuelleInProzentNeu) {
		this.huelleInProzent = zustandHuelleInProzentNeu;
	}

	/**
	 * Liefert des Wert des Lebenserhaltungssystemes in Prozent
	 * @return gibt den Wert des Lebenserhaltungssystemes in Prozent des
	 *         Raumschiffes vom Datentyp int zur�ck
	 */
	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}

	/**
	 * Setzt den Wert des Lebenserhaltungssystemes in Prozent
	 * @param zustandLebenserhaltungssystemInProzentNeu Zustand des
	 *                                                  Lebenserhaltungssystemes in
	 *                                                  Prozent
	 */
	public void setLebenserhaltungssystemInProzent(int zustandLebenserhaltungssystemInProzentNeu) {
		this.lebenserhaltungssystemInProzent = zustandLebenserhaltungssystemInProzentNeu;
	}

	/**
	 * Liefert die Anzahl der Androiden zurueck
	 * @return gibt die Anzahl der Androiden des Raumschiffes vom Datentyp int
	 *         zur�ck
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	/**
	 * Setzt die Anzahl der Androiden
	 * @param androidenAnzahl Anzahl der Androiden
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	/**
	 * Liefert den Schiffsnamen zurueck
	 * @return gibt den Schiffsnamen vom Datentyp String zur�ck
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	/**
	 * Setzt den Schiffsnamen
	 * @param schiffsname Name des Raumschiffes
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	/**
	 * Fuegt dem Raumschiff ein Objekt neueLadung der Klasse Ladung hinzu. 
	 * Das uebergebende Objekt neueLadung
	 * wird dem Ladungsverzeichnis vom Datentyp ArrayList hinzugefuegt
	 * 
	 * @param neueLadung hinzuzfuegende Ladung
	 * 
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	/**
	 * Schiesst einen Photonentorpedo auf ein uebergebendes Objekt der Klasse
	 * Raumschiff
	 * 
	 * Gibt es keine Photonentorpedos, wird "-=*Click*=-" vom Datentyp String als
	 * Argument an die Methode nachrichAnAlle uebergeben.
	 * 
	 * Ansonsten wird die Anzahl der Torpedos um 1 reduziert und "Photonentorpedo
	 * abgeschossen" vom Datentyp String wird der Methode nachrichAnAlle uebergeben.
	 * Ebenfalls wird die Methode treffer aufgerufen.
	 * 
	 * 
	 * @param raumschiff Name des Raumschiffes auf welches geschossen werden soll
	 */
	public void photonentorpedoSchiessen(Raumschiff raumschiff) {
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		}

		else {
			photonentorpedoAnzahl -= 1;
			nachrichtAnAlle("[" + this.getSchiffsname() + "]>>> " + "Photonentorpedo abgeschossen");
			treffer(raumschiff);
		}
	}

	/**
	 * Schiesst mit der Phaserkanone auf ein uebergebendes Objekt der Klasse
	 * Raumschiff Ist die Energieversorgung des Raumschiffes unter 50 Prozent wird
	 * "-=*Click*=-" vom Datentyp String der Methode nachrichAnAlle uebergeben.
	 * 
	 * Ansonsten wird die Energieversorgung um 50 Prozent reduziert und
	 * "Phaserkanone abgeschossen" vom Datentyp String wird der Methode
	 * nachrichtAnAlle zusammen mit dem Schiffsnamen (ueber die Methode
	 * "getSchiffsname()") uebergeben. Ebenfalls wird die Methode treffer aufgerufen
	 * 
	 * @param raumschiff Name des Raumschiffes auf welches geschossen werden soll
	 */
	public void phaserkanoneSchiessen(Raumschiff raumschiff) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}

		else
			energieversorgungInProzent -= 50;
		nachrichtAnAlle("[" + this.getSchiffsname() + "]>>> " + "Phaserkanone abgeschossen");
		treffer(raumschiff);
	}

	/**
	 * Vermerkt einen Treffer an einem uebergebenden Objekt der Klasse Raumschiff.
	 * Auf der Konsole wird der Schiffsname mit der Information "wurde getroffen"
	 * ausgegeben. Der Wert der Schilde des getroffenen Raumschiffes wird ueber eine setter-Methode um 50
	 * Prozent gesenkt. Sollte die Schilde vollstaendig zerstoert worden sein -Pruefung ueber die if-Schleife - (Wert
	 * <=0) wird ausserdem die Huelle des Raumschiffes sowie die Energieversorgung
	 * um 50 Prozent gesenkt. Ist der Zustand der Huelle <=0 -Pruefung ueber die if-Schleife -, wird die Methode
	 * nachrichtAnAlle aufgerufen und der Schiffsnamen sowie der Text "Alle
	 * Lebenserhaltungssysteme sind zerst�rt !!!" �bergeben.
	 * 
	 * @param raumschiff Name des getroffenen Raumschiffes
	 */
	private void treffer(Raumschiff raumschiff) {
		System.out.println("[" + raumschiff.getSchiffsname() + "] wurde getroffen !");

		raumschiff.setSchildeInProzent(raumschiff.getSchildeInProzent() - 50);

		if (raumschiff.getSchildeInProzent() <= 0) {
			raumschiff.setHuelleInProzent(raumschiff.getHuelleInProzent() - 50);
			raumschiff.setEnergieversorgungInProzent(raumschiff.getEnergieversorgungInProzent() - 50);

			if (raumschiff.getHuelleInProzent() <= 0) {
				nachrichtAnAlle("[" + this.getSchiffsname() + "]: " + "Alle Lebenserhaltungssysteme sind zerst�rt !!!");
			}
		}
	}

	/**
	 * Die Methode fuegt dem Broadcast Kommunikator vom Typ ArrayList eine
	 * uebergebende Nachricht vom Datentyp String hinzu. Die Nachricht wird auf der
	 * Konsole ausgegeben.
	 * 
	 * @param message Nachrichtentext vom Datentyp String
	 */
	public void nachrichtAnAlle(String message) {

		broadcastKommunikator.add(message);
		System.out.println(message);
	}

	/**
	 * Diese statische Methode gibt den Inhalt des Broadcast Kommunikators vom
	 * Datentyp ArrayList zur�ck. Der Inhalt wird in der Konsole angezeigt.
	 * 
	 * @return gibt den Inhalt des broadcastKommunikator vom Datentyp ArrayList
	 *         zurueck
	 */
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {

		System.out.println(broadcastKommunikator);

		return broadcastKommunikator;
	}

	/**
	 * Methode stellt Photonentorpedos bereit. Der Parameter der einzusetzenden
	 * Torpedos wird der Methode als anzahlTorpedos vom Typ int uebergeben. Wenn
	 * keine Photonentorpedos in der Ladung oder auf dem Schiff verfuegbar sind,
	 * gibt es eine Meldung in der Konsole und es wird der aufgerufenen Methode
	 * nachrichtAnAlle ein "-=*Click*=-" vom Datentyp String uebergeben. Wenn Anzahl
	 * der einzusetzenden Torpedos groesser ist als die Menge der aktuell
	 * vorhandenen, werden alle verfuegbaren Torpedos eingesetzt. Ansonsten wird die
	 * Ladungsmenge der Photonentorpedos vermindert und die Anzahl der Torpedos im
	 * Raumschiff erhoeht. Die Anzahl der eingesetzten Torpedos wird in der Konsole
	 * ausgegeben.
	 * 
	 * @param anzahlTorpedos Anzahl der Torpedos die bereitgestellt/eingesetzt
	 *                       werden (sollen)
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) { // anzahlTorpedos = anzahl der Tordpedos die bereitgestellt
															// werden sollen -->> **methode wurde mit hilfe gel�st
															// (FIAE-Azubi)**

		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getBezeichnung().equals("Photonentorpedo")) {
				if (ladungsverzeichnis.get(i).getMenge() < anzahlTorpedos) {
					int gesamtAnzahlPhotonentorpedos = ladungsverzeichnis.get(i).getMenge()
							+ getPhotonentorpedoAnzahl();
					setPhotonentorpedoAnzahl(gesamtAnzahlPhotonentorpedos);
					System.out.println(
							"[" + ladungsverzeichnis.get(i).getMenge() + "]" + " Photonentorpedo(s) eingesetzt");
					ladungsverzeichnis.get(i).setMenge(0);
				} else {
					ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge() - anzahlTorpedos);
					setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedos);
					System.out.println("[" + anzahlTorpedos + "]" + " Photonentorpedo(s) eingesetzt");
				}
				return;
			}
		}
		System.out.println("Keine Photonentorpedos gefunden!");
		nachrichtAnAlle("-=*Click*=-");
	}

	/**
	 * Methode entscheidet mit den uebergebenden Parametern der Datentypen boolean
	 * (schutzschilde, energieversorgung, schiffshuelle) sowie der anzahlDroiden vom
	 * Datentyp int durch Berechnung eines Reparaturwertes mit Hilfe einer
	 * generierten Zufallszahl (Math.random) welche Schiffstrukturen zu reparieren
	 * sind. Das Ergebnis des generierten repWerts vom Typ int wird den auf true
	 * gesetzten Schiffstsrukturen vom Typ boolean mit einer setter-Methode
	 * hinzugefuegt.
	 * 
	 * Ist die Anzahl der uebergebenden Androiden groesser als die vorhandene Anzahl
	 * der Androiden im Raumschiff, wird die vorhandene Anzahl der Androiden
	 * eingesetzt.
	 * 
	 * @param schutzschilde     ist Schutzschilde reparaturbeduerftig vom Datentyp
	 *                          boolean - true = ja
	 * @param energieversorgung ist Energieversorgung reparaturbeduerftig vom
	 *                          Datentyp boolean - true = ja
	 * @param schiffshuelle     ist Schiffshuelle reparaturbeduerftig vom Datentyp
	 *                          boolean - true = ja
	 * @param anzahlDroiden     Anzahl der einzusetzenden Reparatur-Androiden vom
	 *                          Datentyp int
	 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle,
			int anzahlDroiden) {

		int zufallszahl = (int) (Math.random() * 100); // *100 = Zufallszahl zwischen 0 und 100 wird generiert, (int)
														// --> Math.random generiert double, (int) wandelt dabei von
														// double in int um (explizite Typumwandlung)
		if (anzahlDroiden > getAndroidenAnzahl()) { // -->> **methode wurde mit hilfe gel�st (FIAE-Azubi)**
			anzahlDroiden = getAndroidenAnzahl();
		}
		int a = 0;
		int b = 0;
		int c = 0;
		if (schutzschilde == true) {
			a = 1;
		}
		if (energieversorgung == true) {
			b = 1;
		}
		if (schiffshuelle == true) {
			c = 1;
		}

		int anzahlTrue = a + b + c;
		int repWert = zufallszahl * anzahlDroiden / anzahlTrue;

		if (schutzschilde == true) {
			setSchildeInProzent(getSchildeInProzent() + repWert);
		}
		if (energieversorgung == true) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + repWert);
		}
		if (schiffshuelle == true) {
			setHuelleInProzent(getHuelleInProzent() + repWert);
		}

	}

	/**
	 * Gibt den aktuellen Zustand des Raumschiffes mit Hilfe der getter-Methoden der
	 * einzelnen Attribute auf der Konsole aus.
	 */
	public void zustandRaumschiff() {
		System.out.println("Photonentorpedos (Anzahl): " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung (in Prozent): " + getEnergieversorgungInProzent());
		System.out.println("Schilde (in Prozent): " + getSchildeInProzent());
		System.out.println("Huelle (in Prozent): " + getHuelleInProzent());
		System.out.println("Lebenserhaltungssystem (in Prozent): " + getLebenserhaltungssystemInProzent());
		System.out.println("Schiffsname: " + getSchiffsname());
		System.out.println("Androiden (Anzahl): " + getAndroidenAnzahl());

	}

	/**
	 * Gibt den Inhalt des ladungsverzeichnis vom Typ ArrayList auf der Konsole aus.
	 * Ausgegebn werden der Schiffsname vom Datentyp String, die Bezeichnung vom
	 * Datentyp int als auch die Menge der Ladung vom Datentyp int durch die
	 * getter-Methoden.
	 */
	public void ladungsverzeichnisAusgeben() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {

			System.out.println("ladungsverzeichnis " + "[" + this.getSchiffsname() + "]: " + "Art der Ladung: "
					+ ladungsverzeichnis.get(i).getBezeichnung() + " Menge: " + ladungsverzeichnis.get(i).getMenge());

		}
	}

	/**
	 * Raeumt das Ladungsverszeichnis vom Typ ArrayList auf. Ist die Ladungsmenge
	 * vom Datentyp int in dem ladungsverzeichnis = 0, wird diese Ladung aus dem
	 * Ladungsverzeichnis entfernt.
	 */
	public void landungsverzeichnisAufraeumen() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getMenge() == 0) {
				ladungsverzeichnis.remove(i);
				i--;
			}
		}

	}
}
