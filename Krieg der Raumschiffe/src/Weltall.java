/**
 * Die Klasse Weltall dient als Testklasse.
 * @author Mathias Hellmich
 * @version 1 vom 28.04.2021
 *
 */
public class Weltall {
	/**
	 * Die Main-Methode hat die Funktion die einzelnen Objekte der Klassen Raumschiff und Ladung
	 * sowie der darin generierten Methoden zu testen.
	 * 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Heghta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "NiVar");

		Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schwert = new Ladung("Batleth Klingonen Schwert", 200);
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		Ladung roteMaterie = new Ladung("Rote Materie", 2);
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);

		klingonen.addLadung(schneckensaft);
		klingonen.addLadung(schwert);

		romulaner.addLadung(borgSchrott);
		romulaner.addLadung(roteMaterie);
		romulaner.addLadung(plasmaWaffe);

		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(photonentorpedo);

		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println();

		romulaner.phaserkanoneSchiessen(klingonen);

		System.out.println();

		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch !");

		System.out.println();

		klingonen.zustandRaumschiff();
		System.out.println();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println();

		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		System.out.println();
		vulkanier.photonentorpedosLaden(3);
		System.out.println();
		vulkanier.landungsverzeichnisAufraeumen();

		System.out.println();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);

		System.out.println();

		klingonen.zustandRaumschiff();
		System.out.println();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println();

		romulaner.zustandRaumschiff();
		System.out.println();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println();

		vulkanier.zustandRaumschiff();
		System.out.println();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println();

//		Raumschiff.eintraegeLogbuchZurueckgeben();

	}

}
