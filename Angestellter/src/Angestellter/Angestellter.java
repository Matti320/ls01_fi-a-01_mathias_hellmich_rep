package Angestellter;

public class Angestellter {

	 private String name;
	 private String vorname;
	 private double gehalt;
	
	 
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.

	 
	 /**
	  * Dies ist der Konstruktor f�r ein Angestelltenobjekt
	  * @param vorname (String)
	  * @param name (String)
	  * @param gehalt (int)
	  * Bitte Vor-, Nachnamen und Gehalt eintragen.
	  */
	 public Angestellter(String vorname, String name, double gehalt) {
		 
		 this.name = name;
		 this.vorname = vorname;
		 this.gehalt = gehalt;
		 
	 }
	 
	 public Angestellter(String vorname, String name) {
		 
		 this.name = name;
		 this.vorname = vorname; 
		 
	 }
	 
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 public String getName() {
	    return name;
	 }

	 public void setVorname(String vorname) {
		 this.vorname = vorname;
	 }
	 
	 public String getVorname() {
		 return vorname;
	 }
	 
	 public void setGehalt(double gehalt) {
		 if (gehalt<0){
			 this.gehalt=555;
			 System.out.println("Das Gehalt darf nicht Negativ sein !!!");
			 System.out.println("Der Mindestlohn (555 �) wird gesetzt.");
		 }
		 else
		 this.gehalt = gehalt;
	   //TODO: 1. Implementieren Sie die entsprechende set-Methoden. 
	   //Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
	 }
 
	 public double getGehalt() {
	   //TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		 return gehalt;
	 }
	 
	 //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
	 
	 public String vollname() {
		
		 return this.vorname+this.name;
	 }
}
	